<!DOCTYPE html>
<!--
Author:  Michal Bača, Martin Strašil
Login:   xbacam02, xstras02
File:	index
Date:	20.4.2015
-->

<?php
require("./utils/pageComponents.php");
require("./utils/tables.php");
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="utils/css/style.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/jqueryui/css/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.core.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.metro.css">

        <script language="javascript" type="text/javascript" src="utils/js/libs/jquery/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/jqueryui/jquery-ui.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.sort.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.filter.js"></script>

        <script>
            $(document).ready(function () {
                // $("#content").dialog();
               // $("input[name=loginSubmit]").button();
                
                $('#cTable').footable();

                $("#filter").change(function () {
                    var filtr = $('#cTable').data('footable-filter');
                    filtr.filter($("#filter").val());
                });
            });
        </script>


    </head>
    <body>
        <?php
        // Inialize session
        session_start();

        if (isset($_GET["logout"])) {
            if (htmlspecialchars($_GET["logout"]) == '1')
            {
                unset($_SESSION['login']);
                unset($_SESSION['admin']);
            }
        }

        $login = isset($_GET["login"])  ? false : true;
        if (isset($_SESSION['admin']))
            $admin = ($_SESSION['admin'] != '1') ? false : true;
        else
            $admin = false;
        putHeader($login, C_INDEX, $admin);
        ?>

        <div id="content">
            <p>
                <?php
                if (isset($_GET["login"])) {
                    putTable(C_INDEX, true, "Index");
                }
                ?>



            <p>






        </div>

        <?php putFooter() ?>

    </body>
</html>
