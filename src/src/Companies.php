<!DOCTYPE html>
<!--
Author:	Michal Bača
Login:	xbacam02 
File:	Companies
Date:	3.4.2015
-->
<?php
require("./utils/pageComponents.php");
require("./utils/tables.php");

session_start();
if (empty($_SESSION["login"]))
    header('Location: index.php');
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="utils/css/style.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/jqueryui/css/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.core.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.metro.css">

        <script language="javascript" type="text/javascript" src="utils/js/libs/jquery/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/jqueryui/jquery-ui.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.sort.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.filter.js"></script>


        <script>
            $(document).ready(function () {
                $("#addNewCompany").button();
                $("#addNewCompany").click(function () {
                    $("#newDialog .actionT").val('10');

                    $("#newDialog .fId").val("");
                    $("#newDialog .fCompanyName").val("");
                    $("#newDialog .fCompanyAdress").val("");
                    $("#newDialog .fContact").val("");
                    $('#newDialog').trigger("reset");

                    $("#newDialog").dialog("option", "title", "Přidat novou společnost");
                    $("#newDialog").dialog("open");
                });

                $("#newDialog").dialog({
                    autoOpen: false,
                    modal: true, buttons: {
                        "Uložit": function () {
                            //alert("yes");
                            $("#newDialog form").submit();
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }});

                $("#delDialog").dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                        "Delete": function () {
                            //alert("yes");
                            $("#delDialog form").submit();
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });


                $('#cTable').footable();

                $("#filter").change(function () {
                    var filtr = $('#cTable').data('footable-filter');
                    filtr.filter($("#filter").val());
                });

                $(".rEdit").click(function () {
                    $('#newDialog').trigger("reset");
                    $("#newDialog .fCompanyName").val("");
                
                    var row = $(this).closest("tr").css("background-color", "red");

                    var id = row.children("td.tId").text();
                    var name = row.children("td.tName").text();
                    var logoName = row.children("td.tLogo").text();
                    var adress = row.children("td.tAdress").text();
                    var contact = row.children("td.tContact").text();

                    $("#newDialog .actionT").val("11");

                    $("#newDialog .fId").val(id);
                    $("#newDialog .fCompanyName").val(name);
                    $("#newDialog .fCompanyAdress").val(adress);  
                    $("#fSelect").val(contact);
   
                    $("#newDialog").dialog("option", "title", "Upravit firmu");
                    $("#newDialog").dialog("open");
                });

                $(".rDelete").click(function () {
                    var row = $(this).closest("tr").css("background-color", "yellow");

                    $("#delMessage").text("");
                    var name = row.children("td.tName").text();
                    var id = row.children("td.tId").text();
                    
                    $("#delDialog .fId").val(id);

                    $("#delMessage").append("Opravdu chcete smazat společnost " + name + " (" + id + ")?");
                    $("#delDialog").dialog("option", "title", "Smazat společnost " + name);
                    $("#delDialog").dialog("open");

                });
            });
        </script>

    </head>
    <body>
        <?php
        putHeader(false, C_COMPANIES)
        ?>

        <div id="content">

            <?php
            putTable(C_COMPANIES, true, "Společnosti");
            ?>
            <input type="submit" id="addNewCompany" value="Přidat novou společnost">
            <?php
            putDialog(C_COMPANIES, M_DELETE, "delDialog");
            putDialog(C_COMPANIES, M_NEW, "newDialog");
            ?>

        </div>

        <?php putFooter() ?>
    </body>
</html>
