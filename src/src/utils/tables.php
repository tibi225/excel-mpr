<?php

/*
 * Author:   Michal Bača, Martin Strašil
 * Login:   xbacam02, xstras02
 * File:	tables.php
 * Date:	26.4.2015
 */

//require("./pageComponents.php");

class Company {

    var $logo;
    var $name;
    var $adress;
    var $contact;
    var $id;

    function Company($id, $logo, $name, $adress, $contact, $contactName) {
        $this->logo = $logo;
        $this->name = $name;
        $this->adress = $adress;
        $this->contact = $contact;
        $this->contactName = $contactName;
        $this->id = $id;
    }

}

//Contract->type
const CT_TYP1 = 100;
const CT_TYP2 = 101;

class Contract {
    var $id;
    var $type;
    var $company;
    var $companyName;
    var $contact;
    var $contactName;
    var $state;
    
    function Contract($id, $type, $company, $name, $contact, $contactName, $state) {
        $this->id = $id;
        $this->type = $type;
        $this->company = $company;
        $this->contact = $contact;
        $this->state = $state;
        $this->companyName = $name;
        $this->contactName = $contactName;
    }
    
}

class Contact {
    var $id;
    var $name;
    var $password;
    
    function Contact($id, $name, $pass) {
        $this->id = $id;
        $this->name = $name;
        $this->password = $pass;
        
    }
    
}

class SpokePerson {
    var $id;
    var $name;
    var $text;
    
    function SpokePerson($idd, $sName, $sText) {
        $this->id = $idd;
        $this->name = $sName;
        $this->text = $sText;
        
    }
    
}

function putTableHead($type, $caption) {
    echo 'Filtr: <input type="text" id="filter" class="filter">';
    echo '<table id="cTable" class="footable" data-filter="#filter">

          <caption>'.$caption.'</caption>  <thead><tr>';

    switch ($type) {
        case C_INDEX:
            echo '<th><span>Logo</span></th> <th><span>Název</span></th>';
            break;
        case C_CONTRACTS:
            echo '<th><span>ID</span></th> <th><span>Typ</span></th> <th>Společnost</th><th class="hidden">Společnost ID</th>  <th>Kontakt</th>  <th class="hidden">Kontakt ID</th> <th>Stav</th><th colspan=2>Akce</th>';
            break;
        case C_CONTACTS:
            echo '<th><span>ID</span></th> <th><span>Jméno</span></th> <th>Heslo</th><th colspan=2>Akce</th>';
            break;
        case C_COMPANIES:
            echo '<th>Company id</td></th> <th><span>Název</span></th> <th>Adresa</th> <th class="hidden">Kontakt ID</th> <th>Kontakt</th><th><span>Logo</span> <th colspan=3>Akce</th>';
            break;
        case C_SPOKEPERSON:
             echo '<th><span>ID</span></th> <th><span>Jméno</span></th> <th>Stav/Role</th><th colspan=2>Akce</th>';
            break;
    }

    echo '</tr></thead><tbody>';
}

function closeTable() {
    echo '</tbody></table>';
}

/**
 * vytvořit objekty (company, Contacts etc) pro každou tabulku a  předat jako parameetr
 */
function putTableRow($type, $obj) {
    echo '<tr id="'.$obj->id.'">';

    switch ($type) {
        case C_INDEX:
            echo '<td class="tLogo"><image width="80" height="80" src="data:image;base64,'.$obj->logo.'"/></td>'
                .'<td class="tName">' . $obj->name . '</td>';
            break;
        case C_CONTRACTS:
            echo '<td class="tId">'.$obj->id.'</td>'
                .'<td class="tType">'.$obj->type.'</td>'
                .'<td class="tName">'.$obj->companyName.'</td>'
                .'<td class="tCompanyID hidden">'.$obj->company.'</td>'
                .'<td class="tContact">'.$obj->contactName.'</td>'
                .'<td class="tContactID hidden">'.$obj->contact.'</td>'
                .'<td class="tState">'.$obj->state.'</td>';
            break;
        case C_CONTACTS:
            echo '<td class="tId">'.$obj->id.'</td>'
                .'<td class="tName">'.$obj->name.'</td>'
                .'<td class="tPassword">'.$obj->password.'</td>';
            break;
        case C_COMPANIES:
            echo '<td class="tId">' . $obj->id . '</td>'
                .'<td class="tName">' . $obj->name . '</td>'
                .'<td class="tAdress">' . $obj->adress . '</td>'
                .'<td class="tContact hidden">' . $obj->contact . '</td>'
                .'<td class="tContactName">' . $obj->contactName . '</td>'                
                .'<td class="tLogo"><image width="80" height="80" src="data:image/jpeg;base64,'.$obj->logo.'"/></td>';
            break;
        case C_SPOKEPERSON:
             echo '<td class="tId">'.$obj->id.'</td>'
                .'<td class="tName">'.$obj->name.'</td>'
                .'<td class="tState">'.$obj->text.'</td>';
            break;
     
    }
    
     //TODO: ověřit oprávnění a podle toho nagenerovat
    if (isset($_SESSION['admin']))
        $admin = ($_SESSION['admin'] != '1') ? false : true;

    switch ($type) {
        case C_INDEX:
            break;
  
        case C_CONTRACTS:  
        case C_CONTACTS:
        case C_SPOKEPERSON:
            if ($admin)
                echo '<td><a href="#" class="rEdit">Upravit</a></td><td><a href="#" class="rDelete">Smazat</a></td>';
            break;
            
        case C_COMPANIES:
            if ($admin)
                echo '<td><a href="#" class="rEdit">Upravit</a></td><td><a href="#" class="rDelete">Smazat</a></td>';
            echo '<td><a href="CompanyDetail.php?ajax=0&companyID='.$obj->id.'&companyName='.$obj->name.'&companyAdress='.$obj->adress.''
                . '&contact='.$obj->contact.'">Detail</a></td>';
            break;
    }
    echo '</tr>';    
}

/*
 * $type - C_COMPANIES nebo C_CONTACTS nebo C_CONTRACTS
 * $fake - dočasné - vygeneruje smylšenou tabulku - bude nahrazeno reálnými daty
 * $caption - popisek tabulky
 * $companyID - pro výpis spokepersonů dajé společnosti
 */

function putTable($type, $fake, $caption, $companyID = -1) {

    putTableHead($type, $caption);
    // connect to db
    $conn = mysqli_connect('localhost:/var/run/mysql/mysql.sock', 'xstras02', 'ur6mogaj', 'xstras02');
    mysqli_set_charset($conn, 'utf8');
    mysqli_query($conn, "SET NAMES 'utf8';");
    mysqli_query($conn, "SET CHARACTER SET 'utf8';");
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // while have results do        
        switch ($type) {
            case C_INDEX:
                $result = mysqli_query($conn, "SELECT * FROM companies JOIN contacts ON companies.company_contact_id = contacts.contact_id");
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {      
                        $obj = new Company($row["company_id"], base64_encode($row["company_logo"]), $row["company_name"], 
                            $row["company_address"], $row["company_contact_id"],  $row["contact_name"]);
                        putTableRow($type, $obj);
                    }
                }
                break;
            case C_CONTRACTS:
                $result = mysqli_query($conn, "SELECT * FROM contracts JOIN companies ON contracts.contract_company_id = companies.company_id JOIN contacts ON contracts.contract_contact_id = contacts.contact_id ");
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $obj1 = new Contract($row["contract_id"], $row["contract_type"], $row["contract_company_id"], 
                            $row["company_name"], $row["contract_contact_id"], $row["contact_name"], $row["contract_state"]);
                        putTableRow($type, $obj1);
                    }
                }
                break;
            case C_CONTACTS:
                $result = mysqli_query($conn, "SELECT * FROM contacts");
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $obj2 = new Contact($row["contact_id"], $row["contact_name"], $row["contact_password"]);
                        putTableRow($type, $obj2);
                    }
                }
                break;
            case C_COMPANIES:
                $result = mysqli_query($conn, "SELECT * FROM companies JOIN contacts ON companies.company_contact_id = contacts.contact_id");
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $obj = new Company($row["company_id"], base64_encode($row["company_logo"]), $row["company_name"], 
                            $row["company_address"], $row["company_contact_id"],  $row["contact_name"]);
                        putTableRow($type, $obj);
                    }
                }
                break;
            case C_SPOKEPERSON:
                $result =mysqli_query($conn, "SELECT * FROM spokespeople WHERE spokesperson_company_id=".$companyID);
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $obj3 = new SpokePerson($row["spokesperson_id"], $row["spokesperson_name"], $row["spokesperson_contactinfo"]);
                        putTableRow($type, $obj3);
                    }
                }        
                break;
        }
        mysqli_close($conn);
    closeTable();
}


