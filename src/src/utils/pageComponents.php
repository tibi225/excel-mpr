<?php

/*
 * Author:  Michal Bača, Martin Strašil
 * Login:   xbacam02, xstras02
 * File:    pageComponents
 * Date:    26.4.2015
 */
        const C_COMPANIES = 1;
        const C_CONTACTS = 2;
        const C_CONTRACTS = 3;
        const C_INDEX = 4;
        const C_SPOKEPERSON = 5;

        const M_NEW = 10;
        const M_EDIT = 11;
        const M_DELETE = 12;

/*
 * $index - je to stránka index - chceme se přihlásit;
 * $page - aktuální stránka, viz konstanty C_
 * $admin - jedná se o admina ? -> další akce
 */

function putHeader($index = true, $page, $admin = true) {
    
    if (isset($_SESSION['admin']))
        $admin = ($_SESSION['admin'] != '1') ? false : true;

    echo '<div class="logo"><img src="./utils/css/excel-at-fit-logo.png"></div>';
    echo '<div id="header">';
    if ($index) {

        echo '<div id="loginForm"><form method="POST" action="login.php">
                <input type="text" name="login" value="" placeholder="Přihašovací jméno">
                <input type="password" name="password" value="" placeholder="Heslo">
                <input type="submit" name="loginSubmit" value="Příhlaš">
            </form></div>';
    } else {
        echo '<div id="menu"><ul>';
        echo '<li ';
        if ($page == C_INDEX)
            echo "class=menuActive";echo '><a href="index.php?logout=1">Domů</a></li>';           
        echo '<li ';
        if ($page == C_COMPANIES)
            echo "class=menuActive";echo '><a href="Companies.php">Společnosti</a></li>';
        if ($admin) {
            echo '<li ';
            if ($page == C_CONTACTS) {
                echo "class=menuActive";
            } echo '><a href="Contacts.php">Kontakty</a></li>';
        }
        echo '<li ';
        if ($page == C_CONTRACTS)
            echo "class=menuActive";echo '><a href="Contracts.php">Smlouvy</a></li>';
        echo '</ul></div>';
    }

    echo '</div>';    
    echo '<p>Přihlášen jako: ';
    if (isset($_SESSION['login']) and isset($_SESSION['admin'])) {
        echo $_SESSION['login'];
        if ($_SESSION['admin'] == '1')
            echo '[admin]';
    }
    else
    {
        echo 'neznámý uživatel';
    }
    echo  '</p>';
}

function putFooter() {
    echo "<div id=\"footer\">OnTime 2015 | MPR - FIT VUT</div>";
}

/**
 * 
 * @param type $type
 */
//TODO: naplnit z DB ----- doplneno

function putDropDownMenu($type, $id) {
    // connect to db
    $conn = mysqli_connect('localhost:/var/run/mysql/mysql.sock', 'xstras02', 'ur6mogaj', 'xstras02');
    mysqli_set_charset($conn, 'utf8');
    mysqli_query($conn, "SET NAMES 'utf8';");
    mysqli_query($conn, "SET CHARACTER SET 'utf8';");
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    switch ($type) {
        case C_CONTACTS:
            $result = mysqli_query($conn, "SELECT * FROM contacts");            
            echo '<select name="'.$id.'">';
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="'.$row["contact_id"].'">' .$row["contact_name"]. '</option>';
                    }
                }
            echo '</select>';
            break;
        case C_COMPANIES:
            $result =  mysqli_query($conn, "SELECT * FROM companies");
            echo '<select name="'.$id.'">';
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="'.$row["company_id"].'">' .$row["company_name"]. '</option>';
                    }
                }
            echo '</select>';
            break;
        case C_CONTRACTS:
            $result =  mysqli_query($conn, "SELECT * FROM contracts");
            echo '<select name="'.$id.'">';
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="'.$row["contract_id"].'">' .$row["contract_type"]. '</option>';
                    }
                }
            echo '</select>';
            break;
    }
    mysqli_close($conn);
}

/**
 * 
 * @param type $type - const C_*
 * @param type $mode - const M_*
 */
function putDialog($type, $mode, $id = "editDialog") {

    echo '<div id="' . $id . '">';

    switch ($type) {
        case C_INDEX:
            break;
        case C_CONTRACTS:
            if ($mode == M_NEW || $mode == M_EDIT) {
                echo '<form method="post" action="submitChanges.php">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>                 
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId"     id="fId"     class="fId"     value=""></input><br>
                 Společnost:<br>';
                putDropDownMenu(C_COMPANIES, "fCompanyNameSelector");
                
                //<input type="text" name="companyName" id="fCompanyName" class="fCompanyName"><br> 
                echo '<br>Typ smlouvy:<br>';
                putDropDownMenu(C_CONTRACTS, "fContractTypeSelector");
                echo '<br>Kontakt:<br> ';
                putDropDownMenu(C_CONTACTS, "fContactSelector");
                //<input type="text" name="contact" id="fContact" class="fContact"><br>
                echo '<br> Stav:<br> <input type="text" name="state" id="fState" class="fState"><br>
                 <br><input type="submit" class="hidden" name="diagSubmit" id="fSubmit"></form><br>';
            } else {
                echo '<form method="post" action="submitChanges.php">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId" id="fId" class="fId" value=""><br><br>
                 <input type="submit" class="hidden" name="diagSubmit" id="fSubmit"></form>
                  <p id="delMessage"></p>';
            }
            break;
        case C_CONTACTS:
            if ($mode == M_NEW || $mode == M_EDIT) {
                echo '<form method="post" action="submitChanges.php">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId"     id="fId"     class="fId"     value=""><br>
                 Jméno:<br><input type="text" name="fName" id="fName" class="fName"><br>              
                 Heslo:<br> <input type="text" name="fPassword" id="fPassword" class="fPassword"><br>
                 <br><input class="hidden" type="submit" name="diagSubmit" id="fSubmit"></form><br>';
            } else {
                echo '<form method="post" action="submitChanges.php">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>                 
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId" id="fId" class="fId" value=""><br>
                 <br><input class="hidden" type="submit" name="diagSubmit" id="fSubmit"></form>
                  <p id="delMessage"></p>';
            }

            break;
        case C_COMPANIES:
            if ($mode == M_NEW || $mode == M_EDIT) {
                echo '<form method="post" action="submitChanges.php" enctype="multipart/form-data">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId"     id="fId"     class="fId"     value=""></input><br>
                 Název:<br><input type="text" name="companyName" id="fCompanyName" class="fCompanyName"><br>
                 Adresa:<br> <input type="text" name="companyAdress" id="fCompanyAdress" class="fCompanyAdress"><br>
                 Kontakt:<br> '; putDropDownMenu(C_CONTACTS, "fContactSelector");
                echo '<br>Logo:<br> <input type="file" name="logo" id="fLogo" class="fLogo"><br>
                 <br><input type="submit"  class="hidden" name="diagSubmit" id="fSubmit"></form><br>';
            } else {
                echo '<form method="post" action="submitChanges.php">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId" id="fId" class="fId" value=""><br>
                 <br><input type="submit" class="hidden" name="diagSubmit" id="fSubmit"></form>
                  <p id="delMessage"></p>';
            }
            break;
        case C_SPOKEPERSON:
            if ($mode == M_NEW || $mode == M_EDIT) {
                echo '<form method="post" action="submitChanges.php">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId"     id="fId"     class="fId"     value=""></input><br>
                 Jméno:<br><input type="text" name="fName" id="fName" class="fName"><br>              
                 Role/stav:<br> <input type="text" name="fStatus" id="fStatus" class="fStatus"><br>
                 <br><input type="submit" class="hidden" name="diagSubmit" id="fSubmit"></form><br>';
            } else {
                echo '<form method="post" action="submitChanges.php">
                 <input type="hidden" name="actionT" id="actionT" class="actionT" value="' . $mode . '"></input><br>
                 <input type="hidden" name="sourceT" id="sourceT" class="sourceT" value="' . $type . '"></input><br>
                 <input type="hidden" name="fId" id="fId" class="fId" value=""><br>
                 <br><input type="submit" class="hidden" name="diagSubmit" id="fSubmit"></form>
                  <p id="delMessage"></p>';
            }
            break;
    }

    echo '</div>';
}
