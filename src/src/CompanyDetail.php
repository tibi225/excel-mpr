<!DOCTYPE html>
<!--
Author:	Michal Bača
Login:	xbacam02 
File:	CompanyDetail
Date:	3.4.2015
-->

<?php
require("./utils/pageComponents.php");
require("./utils/tables.php");


session_start();
if (empty($_SESSION["login"]))
    header('Location: index.php');
?>
<link rel="stylesheet" type="text/css" href="utils/css/style.css">
<link rel="stylesheet" type="text/css" href="utils/js/libs/jqueryui/css/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.core.css">
<link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.metro.css">

<script language="javascript" type="text/javascript" src="utils/js/libs/jquery/jquery.js"></script>
<script language="javascript" type="text/javascript" src="utils/js/libs/jqueryui/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.js"></script>
<script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.sort.js"></script>
<script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.filter.js"></script>


<script>
    $(document).ready(function () {
        $("#addNew").button();
        $("#addNew").click(function () {
            $("#newDialog .actionT").val('10');

             $("#newDialog .fId").val($(".compID").text());
            $("#newDialog .fName").val("");
            $("#newDialog .fStatus").val("");

            $('#newDialog').trigger("reset");


            $("#newDialog").dialog("option", "title", "Přidat nového zástupce");
            $("#newDialog").dialog("open");
        });

        $("#newDialog").dialog({
            autoOpen: false,
            modal: true, buttons: {
                "Uložit": function () {
                    //alert("yes");
                    $("#newDialog form").submit();
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }});

        $("#delDialog").dialog({
            autoOpen: false,
            modal: true,
            buttons: {
                "Delete": function () {
                    //alert("yes");
                    $("#delDialog form").submit();
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });


        $('#cTable').footable();

        $("#filter").change(function () {
            var filtr = $('#cTable').data('footable-filter');
            filtr.filter($("#filter").val());
        });

        $(".rEdit").click(function () {
            $('#newDialog').trigger("reset");
            $("#newDialog .fCompanyName").val("");

            var row = $(this).closest("tr").css("background-color", "red");

            var id = row.children("td.tId").text();
            var name = row.children("td.tName").text();
            var status = row.children("td.tState").text();

            $("#newDialog .actionT").val("11");

            $("#newDialog .fId").val(id);
            $("#newDialog .fName").val(name);
            $("#newDialog .fStatus").val(status);

            $("#newDialog").dialog("option", "title", "Upravit zástupce");
            $("#newDialog").dialog("open");
        });

        $(".rDelete").click(function () {
            var row = $(this).closest("tr").css("background-color", "yellow");

            $("#delMessage").text("");
            var name = row.children("td.tName").text();
            var id = row.children("td.tId").text();

            $("#delDialog .fId").val(id);

            $("#delMessage").append("Opravdu chcete smazat společnost " + name + " (" + id + ")?");
            $("#delDialog").dialog("option", "title", "Smazat společnost " + name);
            $("#delDialog").dialog("open");
        });
    });
</script>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $ajax = false;
        $compID = -1;

        if (isset($_GET["ajax"]) && $_GET["ajax"] == 0) {
            $ajax = false;
        } else {
            $ajax = true;
        }

        if (isset($_GET["companyID"]))
            $compID = $_GET["companyID"];
              echo '<div class="compID hidden"> '.$compID.'</div>';
        putHeader(false, C_COMPANIES)
        ?>


        <div id="content">
            <p>
                <?php
                if (!$ajax) {
                    if (isset($_GET["companyName"])) {
                        echo '<h1>' . $_GET["companyName"] . "</h1>";
                    }

                    if (isset($_GET["companyAdress"])) {
                        echo "<br>Adresa: " . $_GET["companyAdress"];
                    }

                    if (isset($_GET["logo"])) {
                        echo "<br>Logo";
                    }

                    if (isset($_GET["contact"])) {
                        echo "<br>Kontakt: " . $_GET["contact"];
                    }
                }
                ?> 

            </p>

            <?php
            putTable(C_SPOKEPERSON, true, "Kontakty společnosti", $compID);

            if (!$ajax) {
                echo '<input type="submit" id="addNew" value="Přidat nový kontakt">';

                putDialog(C_SPOKEPERSON, M_DELETE, "delDialog");
                putDialog(C_SPOKEPERSON, M_NEW, "newDialog");
            }
            ?>

        </div>

        <?php putFooter() ?>
    </body>
</html>
