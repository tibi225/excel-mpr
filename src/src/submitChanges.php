<?php

/* 
 * Author:	Michal Bača, Martin Strašil
 * Login:	xbacam02, xstras02
 * File:	submitChanges
 * Date:	26.4.2015
 */

/* POST[]
 * Akce -> M_*
 * 
 * DELETE   C_* id
 * ADD NEW  C_* seznam parametru dle typu
 * MODIFY   C_* id seznam parametru
 * 
 * Po dokončení změn přesměrovat zpátky na původní stránku 
 */

//placeholder - z postu určit source (C_*)
require("./utils/pageComponents.php");

		// connect to db
    $conn = mysqli_connect('localhost:/var/run/mysql/mysql.sock', 'xstras02', 'ur6mogaj', 'xstras02');
    mysqli_set_charset($conn, 'utf8');
    mysqli_query($conn, "SET NAMES 'utf8';");
    mysqli_query($conn, "SET CHARACTER SET 'utf8';");
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

//vložit nový záznam
if ($_POST['actionT'] == M_NEW) {
  //novou smlouvu
  if($_POST['sourceT'] == C_CONTRACTS) {
		$sql = mysqli_query($conn, "INSERT INTO contracts (contract_type, contract_state, contract_contact_id, contract_company_id) 
			VALUES ('".$_POST['fContractTypeSelector']."','".$_POST['state']."',".$_POST['fContactSelector'].",".$_POST['fCompanyNameSelector'].")");
  } 
  //nový kontakt
  else if($_POST['sourceT'] == C_CONTACTS) {	
  	$sql = mysqli_query($conn, "INSERT INTO contacts (contact_name, contact_password, admin) 
  		VALUES ('".$_POST['fName']."','".$_POST['fPassword']."',0)");
  } 
  //novou firmu
  else if($_POST['sourceT'] == C_COMPANIES) {
    if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
      $image = addslashes(file_get_contents($_FILES['logo']['tmp_name']));
      $sql = mysqli_query($conn, "INSERT INTO companies (company_name, company_address, company_logo, company_contact_id) 
        VALUES ('".$_POST['companyName']."','".$_POST['companyAdress']."','".$image."',".$_POST['fContactSelector'].")");
    } else {
      $sql = mysqli_query($conn, "INSERT INTO companies (company_name, company_address, company_contact_id) 
        VALUES ('".$_POST['companyName']."','".$_POST['companyAdress']."',".$_POST['fContactSelector'].")");
    }		
  }
  //noveho zastupce firmy
  else if($_POST['sourceT'] == C_SPOKEPERSON) {
  	$sql = mysqli_query($conn, "INSERT INTO spokespeople (spokesperson_name, spokesperson_contactinfo, spokesperson_company_id) 
			VALUES ('".$_POST['fName']."','".$_POST['fStatus']."','".$_POST['fId']."')");
  }
}

//upravit záznam
if ($_POST['actionT'] == M_EDIT ) {
	//smlouvu
  if($_POST['sourceT'] == C_CONTRACTS) {
		$sql = mysqli_query($conn, "UPDATE contracts SET contract_type='".$_POST['fContractTypeSelector']."', contract_state='".$_POST['state']."', 
			contract_contact_id=".$_POST['fContactSelector'].", contract_company_id=".$_POST['fCompanyNameSelector']." WHERE contract_id=".$_POST['fId']);
  } 
  //kontakt
  else if($_POST['sourceT'] == C_CONTACTS) {
  	$sql = mysqli_query($conn, "UPDATE contacts SET contact_name='".$_POST['fName']."', contact_password='".$_POST['fPassword']."' WHERE contact_id=".$_POST['fId']);
  } 
  //firmy
  else if($_POST['sourceT'] == C_COMPANIES) {
    if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
      $image = addslashes(file_get_contents($_FILES['logo']['tmp_name']));
      $sql = mysqli_query($conn, "UPDATE companies SET company_name='".$_POST['companyName']."', company_address='".$_POST['companyAdress']."', 
        company_logo='".$image."' WHERE company_id=".$_POST['fId']);
    } else {
      $sql = mysqli_query($conn, "UPDATE companies SET company_name='".$_POST['companyName']."', company_address='".$_POST['companyAdress']."'
        WHERE company_id=".$_POST['fId']);
    }
  }
  //zastupce firmy
  else if($_POST['sourceT'] == C_SPOKEPERSON) {
		$sql = mysqli_query($conn, "UPDATE spokespeople SET spokesperson_name='".$_POST['fName']."', spokesperson_contactinfo='".$_POST['fStatus']."' 
			WHERE spokesperson_id=".$_POST['fId']);
  }
}

//smazat záznam
if ($_POST['actionT'] == M_DELETE ) {
  //smlouvu
  if($_POST['sourceT'] == C_CONTRACTS) {
  	$sql = mysqli_query($conn, "DELETE FROM contracts WHERE contract_id=".$_POST['fId']);
  } 
  //kontakt
  else if($_POST['sourceT'] == C_CONTACTS) {
		$sql = mysqli_query($conn, "DELETE FROM contacts WHERE contact_id=".$_POST['fId']);
  }
  //firmy
  else if($_POST['sourceT'] == C_COMPANIES) {
		$sql = mysqli_query($conn, "DELETE FROM companies WHERE company_id=".$_POST['fId']);
  }	
  //zástuce firmy
  else if($_POST['sourceT'] == C_SPOKEPERSON) {
		$sql = mysqli_query($conn, "DELETE FROM  spokespeople WHERE spokesperson_id=".$_POST['fId']);
  }
  
}

if (mysqli_query($conn, $sql)) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}
mysqli_close($conn);

$src = "index.php";

if (isset($_POST['sourceT'])) {
    switch ($_POST['sourceT']) {
        case C_COMPANIES:
               $src = "Companies.php"; 
                break;
        case C_SPOKEPERSON:
        	$src = $_SERVER['HTTP_REFERER'];
          break;
        case C_CONTRACTS:
        	$src = "Contracts.php";
          break;
        case C_CONTACTS:
        	$src = "Contacts.php";
          break;
        default:
        	$src = "index.php";
          break;
    }
} else {

    $src = "index.php";
}

header("Location: " . $src);
//edit=ok je poze dočasné; bude nahrazeno
exit();
