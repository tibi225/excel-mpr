<!DOCTYPE html>
<!--
Author:	Michal Bača
Login:	xbacam02 
File:	Contacts
Date:	3.4.2015
-->
<?php
require("./utils/pageComponents.php");
require("./utils/tables.php");

/* TODO: pokud nejsme přihlašení tak přesměrovat na index */
session_start();
if (empty($_SESSION["login"]) or ($_SESSION['admin'] != '1'))
    header('Location: index.php');
// TODO: pokud není admin tak přesměrovat na index
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="utils/css/style.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/jqueryui/css/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.core.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.metro.css">

        <script language="javascript" type="text/javascript" src="utils/js/libs/jquery/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/jqueryui/jquery-ui.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.sort.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.filter.js"></script>

        <script>
            $(document).ready(function () {
                  $("#addNewContact").button();
               $("#addNewContact").click(function () {
                    $("#newDialog .actionT").val('10');

                    $("#newDialog .fId").val("");
                    $("#newDialog .fName").val("");
                    $("#newDialog .fPassword").val("");
                    
                    $('#newDialog').trigger("reset");

                    $("#newDialog").dialog("option", "title", "Přidat nový kontakt");
                    $("#newDialog").dialog("open");
                });

                $("#newDialog").dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                        "Uložit": function () {
                            //alert("yes");
                            $("#newDialog form").submit();
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });

                $("#delDialog").dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                        "Delete": function () {
                            //alert("yes");
                            $("#delDialog form").submit();
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });


                $('#cTable').footable();

                $("#filter").change(function () {
                    var filtr = $('#cTable').data('footable-filter');
                    filtr.filter($("#filter").val());
                });

                $(".rEdit").click(function () {
                    $('#newDialog').trigger("reset");
                    $("#newDialog .fCompanyName").val("");

                    var row = $(this).closest("tr").css("background-color", "red");

                    var id = row.children("td.tId").text();
                    var name = row.children("td.tName").text();

                    var pass = row.children("td.tPassword").text();

                    $("#newDialog .actionT").val("11");

                    $("#newDialog .fId").val(id);
                    $("#newDialog .fName").val(name);
                    $("#newDialog .fPassword").val(pass);

                    $("#newDialog").dialog("option", "title", "Upravit kontakt");
                    $("#newDialog").dialog("open");

                });

                $(".rDelete").click(function () {
                    var row = $(this).closest("tr").css("background-color", "yellow");

                    $("#delMessage").text("");
                    var name = row.children("td.tName").text();
                    var id = row.children("td.tId").text();

                    $("#delDialog .fId").val(id);

                    $("#delMessage").append("Opravdu chcete smazat kontakt " + name + " (" + id + ")?");
                    $("#delDialog").dialog("option", "title", "Smatzat kontakt " + name);
                    $("#delDialog").dialog("open");
                });

            });
        </script>

    </head>
    <body>
        <?php
        putHeader(false, C_CONTACTS)
        ?>

        <div id="content">
            <?php
            putTable(C_CONTACTS, true, "Kontakty");
            ?>
            <input type="submit" id="addNewContact" value="Přidat nový kontakt">
            <?php
            putDialog(C_CONTACTS, M_DELETE, "delDialog");
            putDialog(C_CONTACTS, M_NEW, "newDialog");
            ?>

        </div>

        <?php putFooter() ?>
    </body>
</html>
