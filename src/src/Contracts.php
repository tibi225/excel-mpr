<!DOCTYPE html>
<!--
Author:	Michal Bača
Login:	xbacam02 
File:	Contracts
Date:	3.4.2015
-->

<?php
require("./utils/pageComponents.php");
require("./utils/tables.php");
/* TODO: pokud nejsme přihlašení tak přesměrovat na index */
session_start();
if (empty($_SESSION["login"]))
    header('Location: index.php');
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="utils/css/style.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/jqueryui/css/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.core.css">
        <link rel="stylesheet" type="text/css" href="utils/js/libs/FooTable-2/css/footable.metro.css">

        <script language="javascript" type="text/javascript" src="utils/js/libs/jquery/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/jqueryui/jquery-ui.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.sort.js"></script>
        <script language="javascript" type="text/javascript" src="utils/js/libs/FooTable-2/js/footable.filter.js"></script>

        <script>
            $(document).ready(function () {
                 $("#addNewContract").button();
                $("#addNewContract").click(function () {
                    $("#newDialog .actionT").val('10');
                    $('#newDialog').trigger("reset");
                    $("#newDialog .fId").val("");
                    $("#newDialog .fCompanyName").val("");

                    $("#newDialog .fContact").val("");
                    $("#newDialog .fState").val("");


                    $("#newDialog").dialog("option", "title", "Přidat novou smlouvu");
                    $("#newDialog").dialog("open");
                });

                $("#newDialog").dialog({
                    autoOpen: false,
                    modal: true, buttons: {
                        "Uložit": function () {
                            //alert("yes");
                            $("#newDialog form").submit();
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }}});

                $("#delDialog").dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                        "Delete": function () {
                            //alert("yes");
                            $("#delDialog form").submit();
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });

                $('#cTable').footable();

                $("#filter").change(function () {
                    var filtr = $('#cTable').data('footable-filter');
                    filtr.filter($("#filter").val());
                });

                $(".rEdit").click(function () {
                    $('#newDialog').trigger("reset");

                    var row = $(this).closest("tr").css("background-color", "red");

                    var id = row.children("td.tId").text();
                    var company = row.children("td.tName").text();
                    var companyID = row.children("td.tCompanyID").text();
                    console.log(companyID);
                    var state = row.children("td.tState").text();

                    var contact = row.children("td.tContact").text();
                    var contactID = row.children("td.tContactID").text();

                    var type = row.children("td.tType").text(); //typ smlouvy
             
                    $("#newDialog .actionT").val("11");

                    $("#fContractTypeSelector").val(type);
                    $("#fContactSelector").val(contactID);

                    $("#fCompanyNameSelector").val(companyID);

                    $("#newDialog .fId").val(id);
                    $("#fCompanyName").val(company);


                    $("#newDialog .fContact").val(contact);
                    $("#newDialog .fState").val(state);

                    $("#newDialog").dialog("option", "title", "Upravit firmu");
                    $("#newDialog").dialog("open");

                });

                $(".rDelete").click(function () {
                    var row = $(this).closest("tr").css("background-color", "yellow");

                    $("#delMessage").text("");
                    
                    var state = row.children("td.tState").text();
                    var id = row.children("td.tId").text();
                    
                    $("#delDialog .fId").val(id);
                    $("#delDialog .fState").val(state);
                    
                    $("#delMessage").append("Opravdu chcete smazat tento záznam " + "(" + id + ")" + state + " ?");
                    $("#delDialog").dialog("option", "title", "Smazat záznam ");
                    $("#delDialog").dialog("open");
                });
            });
        </script>

    </head>
    <body>
        <?php
        putHeader(false, C_CONTRACTS)
        ?>

        <div id="content">

            <?php
            putTable(C_CONTRACTS, true, "Smlouvy");
            ?>
            <input type="submit" id="addNewContract" value="Přidat novou smlouvu">
            <?php
            putDialog(C_CONTRACTS, M_DELETE, "delDialog");
            putDialog(C_CONTRACTS, M_NEW, "newDialog");
            ?>


        </div>

        <?php putFooter() ?>
    </body>
</html>
